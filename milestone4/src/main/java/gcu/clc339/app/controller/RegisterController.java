package gcu.clc339.app.controller;

import gcu.clc339.app.business.RegistrationServiceInterface; // Only import the interface
import gcu.clc339.app.models.RegisterModel;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller // Marks this class as a Spring MVC controller
public class RegisterController {

    private final RegistrationServiceInterface registrationService;

    // Constructor-based Dependency Injection
    public RegisterController(RegistrationServiceInterface registrationService) {
        this.registrationService = registrationService;
    }

    // Handles GET requests for the registration page
    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("title", "Register Form");
        model.addAttribute("RegisterModel", new RegisterModel()); // Empty form object for user input
        return "register"; // Returns the view name for the registration form
    }

    // Handles POST requests when the registration form is submitted
    @PostMapping("/register")
    public String processRegistration(@Valid @ModelAttribute("RegisterModel") RegisterModel register, Model model, BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("title", "Register Form");
            return "register";
        }

        // **Use the service to register the user**
        registrationService.registerUser(register);

        return "success";
    }
}
