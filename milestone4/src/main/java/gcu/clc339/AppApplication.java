package gcu.clc339;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("gcu.clc339.app")

@SpringBootApplication
public class AppApplication {

	public static void main(String[] args) {
		
		System.out.println("Milestone 4 before");
		
		SpringApplication.run(AppApplication.class, args);
		
		System.out.println("Milestone 4 after");
	}

}
