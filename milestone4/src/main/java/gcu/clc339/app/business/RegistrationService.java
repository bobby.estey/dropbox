package gcu.clc339.app.business;

import gcu.clc339.app.models.RegisterModel;
import org.springframework.stereotype.Service;

@Service // Marks this class as a Spring-managed service bean
public class RegistrationService implements RegistrationServiceInterface {

    @Override
    public void registerUser(RegisterModel registerModel) {
        // Business logic for registration
        System.out.println("User registered successfully: " + registerModel.getUsername());
    }
}
