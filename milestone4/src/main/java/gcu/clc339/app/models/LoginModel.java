package gcu.clc339.app.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class LoginModel {

	// Username field with validation constraints
	@NotNull(message="Username is a required field") // Ensures the username is not null
	@Size(min=1, max=32, message="Username must be between 1 and 32 characters") // Defines size constraints
	private String username;
	
	// Password field with validation constraints
	@NotNull(message="Password is a required field") // Ensures the password is not null
	@Size(min=1, max=32, message="Password must be between 1 and 32 characters") // Defines size constraints
	private String password;

	// Getter method for username
	public String getUsername() {
		return username;
	}

	// Setter method for username
	public void setUsername(String username) {
		this.username = username;
	}

	// Getter method for password
	public String getPassword() {
		return password;
	}

	// Setter method for password
	public void setPassword(String password) {
		this.password = password;
	}

	// Overriding toString method to display object details
	@Override
	public String toString() {
		return "LoginModel [username=" + username + ", password=" + password + "]";
	}
}

