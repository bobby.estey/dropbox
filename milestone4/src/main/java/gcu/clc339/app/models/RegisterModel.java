package gcu.clc339.app.models;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class RegisterModel 
{
	// First Name field with validation constraints
	@NotEmpty(message = "First Name is required") // Ensures first name is not empty
    @Size(min = 3, max = 32, message = "First Name must be 3-32 characters") // Defines valid length
	private String firstName;
	
	// Last Name field with validation constraints
	@NotEmpty(message = "Last Name is required") // Ensures last name is not empty
    @Size(min = 3, max = 32, message = "Last Name must be 3-32 characters") // Defines valid length
	private String lastName;
	
	// Phone number field with a validation pattern (must be exactly 10 digits)
	@Pattern(regexp = "^[0-9]{10}$", message = "Phone number must be 10 digits") 
	private String phoneNumber;
	
	// Email field with required validation
	@NotEmpty(message = "Email is required") // Ensures email is not empty
    private String email;
	
	// Username field with validation constraints
	@NotEmpty(message = "Username is required") // Ensures username is not empty
    @Size(min = 3, max = 32, message = "Username must be 3-32 characters") // Defines valid length
    private String username;

    // Password field with required validation
    @NotEmpty(message = "Password is required") // Ensures password is not empty
    private String password;

	// Getter and setter methods for each field

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
