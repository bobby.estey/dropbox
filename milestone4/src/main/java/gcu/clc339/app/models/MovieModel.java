package gcu.clc339.app.models;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class MovieModel 
{
	@NotBlank(message="Name is required")
    private String name;

	@NotBlank(message="Author is required")
    private String author;

	@NotBlank(message="Genre is required")
    private String genre;

    @Min(value=0, message="Price must be positive")
    private double price;

    @NotBlank(message="Description is required")
    private String description;

    @Min(value=1, message="Stock quantity must be at least one")
    private int stockQuantity;

    @NotNull(message="Release date is required")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date releaseDate;
	
	public MovieModel()
	{
		name = "";
		author = "";
		genre = "";
		price = 0;
		description = "";
		stockQuantity = 0;
		releaseDate = new Date();
	}
	
	public MovieModel(String name, String author, String genre, double price, String description, int stockQuantity,
			Date releaseDate) {
		this.name = name;
		this.author = author;
		this.genre = genre;
		this.price = price;
		this.description = description;
		this.stockQuantity = stockQuantity;
		this.releaseDate = releaseDate;
	}
	
	
//	public  int compareTo(MovieModel another)
//	{
//		MovieModel original = new MovieModel();
//		String title1 = original.name;
//		String title2 = original.name;
//		int titleComparioson = title1.compareTo(title2);
//		
//		if(titleComparioson < 0)
//		{
//			System.out.println(title1 + " is before " + title2 + "alphabetically");
//		}
//		else if(titleComparioson > 0)
//		{
//			System.out.println(title2 + " is before " + title1 + "alphabetically");
//		}
//		else
//		{
//			System.out.println(title1 + " and " + title2 + " are tied alphabetically");
//		}
//		
//		double price1 = original.price;
//		double price2 = original.price;
//		
//		if(price1 > price2)
//		{
//			System.out.println(price1 + " is greather than " + price2);
//		}
//		else if(price1 < price2)
//		{
//			System.out.println(price1 + " is less than " + price2 );
//		}
//		else
//		{
//			System.out.println(price1 + " and " + price2 + " are equal");
//		}
//		
//		int qty1 = original.stockQuantity;
//		int qty2 = original.stockQuantity;
//
//		if(qty1 > qty2)
//		{
//			System.out.println(qty1 + " is greather than " + qty2);
//		}
//		else if(qty1 < qty2)
//		{
//			System.out.println(qty1 + " is less than " + qty2 );
//		}
//		else
//		{
//			System.out.println(qty1 + " and " + qty2 + " are equal");
//		}
//		
//		return -1;
//	}
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getAuthor() {
		return author;
	}



	public void setAuthor(String author) {
		this.author = author;
	}



	public String getGenre() {
		return genre;
	}



	public void setGenre(String genre) {
		this.genre = genre;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getStockQuantity() {
		return stockQuantity;
	}



	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}



	public Date getReleaseDate() {
		return releaseDate;
	}



	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
}

