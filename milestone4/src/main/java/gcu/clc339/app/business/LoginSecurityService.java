package gcu.clc339.app.business;

import org.springframework.stereotype.Service;

@Service // Spring Service 
public class LoginSecurityService 
{
	// Do a Authentication Check with hard coded username and password for now "test"
	public boolean authenticate (String username, String password)
	{
		if ("test".equals(username) && "test".equals(password))
		{
			// If the credentials match return true for login 
			System.out.println("Successful login with Username: " + username);
			return true;
		}
		
		// If credentials do not match return false
		System.out.println("The Username or Password are incorrect.");
		return false;
		
	}
}
