package gcu.clc339.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import gcu.clc339.app.business.LoginSecurityService;
import gcu.clc339.app.models.LoginModel;
import gcu.clc339.app.models.OrderModel;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;

@Controller
@RequestMapping("/login")
public class LoginController 
{
	
	@Autowired
	private LoginSecurityService security;

	/**
	 * Simple Hello World Controller that returns a View Name along with a Model
	 * Attribute named message. Invoke using /test2 URI.
	 * 
	 * @param model Model to bind to the View.
	 * 
	 * @return View name hello
	 */
	@GetMapping("/")
	public String display(Model model) {

		model.addAttribute("title", "Login Form");
		model.addAttribute("loginModel", new LoginModel());
		return "login";
	}

	// Bind to the LoginModel Bean
	// @Valid - looks at the Bean for Size validation
	@PostMapping("/doLogin")
	public String doLogin(@Valid LoginModel loginModel, BindingResult bindingResult, Model model, HttpSession session) {

		// Username and password Validation Check 
		if (bindingResult.hasErrors())
		{
			model.addAttribute("title", "Login Form");
			return "login";	
		}
		
		// Username and password authentication check (using hardcore for now)
		if (!security.authenticate(loginModel.getUsername(), loginModel.getPassword()))
		{
			// if there is an authentication error then display this message
			model.addAttribute("autherror", "Invalid username or password");
			
			// return login view with authentication message
			return "login";
		}
		
		// Storing the logged in user's username in the session 
		session.setAttribute("user", loginModel.getUsername());
		
		
		List<OrderModel> orders = new ArrayList<OrderModel>();
		orders.add(new OrderModel(0L, "0000000000", "Product 0", 0.00f, 0));
		orders.add(new OrderModel(1L, "0000000001", "Product 1", 1.00f, 1));
		orders.add(new OrderModel(2L, "0000000002", "Product 2", 2.00f, 2));
		orders.add(new OrderModel(3L, "0000000003", "Product 3", 3.00f, 3));
		orders.add(new OrderModel(4L, "0000000004", "Product 4", 4.00f, 4));

		// Print the form values out
		System.out.println(String.format("Form with Username of %s and Password of %s", loginModel.getUsername(),
				loginModel.getPassword()));

		model.addAttribute("title", "My DVDS");

		model.addAttribute("orders", orders);
		return "orders";
	}

}
