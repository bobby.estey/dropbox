package gcu.clc339.app.business;

import gcu.clc339.app.models.RegisterModel;

public interface RegistrationServiceInterface {
    void registerUser(RegisterModel registerModel);
}
