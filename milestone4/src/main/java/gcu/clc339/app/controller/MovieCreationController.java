package gcu.clc339.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import gcu.clc339.app.models.MovieModel;
import jakarta.validation.Valid;

@Controller
@RequestMapping("/newMovie")
public class MovieCreationController {
	// Handles GET requests for the registration page
	@GetMapping("/")
	public String showDVDCreationForm(Model model) {
		model.addAttribute("title", "New DVD");
		model.addAttribute("movieModel", new MovieModel());
		return "newMovie";
	}

	// Handles POST requests when the registration form is submitted
	@PostMapping("/createMovie")
	public String createMovie(@Valid @ModelAttribute("movieModel") MovieModel movieModel, 
            BindingResult bindingResult, 
            Model model) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("title", "New Movie Form");
			return "newMovie";
		}
	

		// **Use the service to register the user**
//        registrationService.registerUser(register);

		return "movieSuccess";
	}

	// end
}
