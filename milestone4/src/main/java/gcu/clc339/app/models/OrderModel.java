package gcu.clc339.app.models;

public class OrderModel {

    // Unique identifier for the order
    private Long id;
    
    // Order number associated with the order
    private String orderNo;
    
    // Name of the product being ordered
    private String productName;
    
    // Price of the product
    private float price;
    
    // Quantity of the product ordered
    private int quantity;

    // Constructor to initialize OrderModel with given values
    public OrderModel(Long id, String orderNo, String productName, float price, int quantity) {
        super();
        this.id = id;
        this.orderNo = orderNo;
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
    }

    // Getter method for order ID
    public Long getId() {
        return id;
    }

    // Setter method for order ID
    public void setId(Long id) {
        this.id = id;
    }

    // Getter method for order number
    public String getOrderNo() {
        return orderNo;
    }

    // Setter method for order number
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    // Getter method for product name
    public String getProductName() {
        return productName;
    }

    // Setter method for product name
    public void setProductName(String productName) {
        this.productName = productName;
    }

    // Getter method for product price
    public float getPrice() {
        return price;
    }

    // Setter method for product price
    public void setPrice(float price) {
        this.price = price;
    }

    // Getter method for product quantity
    public int getQuantity() {
        return quantity;
    }

    // Setter method for product quantity
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}


